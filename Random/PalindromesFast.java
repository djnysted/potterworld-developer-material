
public class PalindromesFast {
	/*
	 * I'm going to explain a lot of this in a comment block up here, because this
	 * implementation is weirrrrrd. Instead of relying on multiplication to find
	 * a palindrome, this implementation flips this on it's head so that we use
	 * the palindrome to find the factors. This makes use of several pieces of knowledge
	 * we have from the problem:
	 * 
	 * 1. We're looking for the largest palindrome with 7 digit factors. 
	 * 2. The largest number we can have to work with is 99999980000001. This means that the
	 * largest palindrome we can possibly have is 99999899899999. We can use this as a starting
	 * point for parsing a palindrome. 
	 * 3. The next smallest palindrome is equal to the front half of a palindrome, minus 1, mirrored.
	 * 4. When finding factors, if one factor goes above 7 digits, we can immediately rule out that
	 * palindrome. 
	 * 5. When we find a palindrome with factors that are both 7 digits, we can immediately exit as
	 * we're working in reverse.
	 * 
	 * With this knowledge, we can explore how this program works. We start with our largest possible palindrome
	 * and check this for factors, entering a while loop. NextLargestPalin proceeds to take that palindrome
	 * and split it in half, then generate a new palindrome essentially 1 smaller than the previous (if
	 * our numeral system was exclusively palindromes, anyhow). From there, we repeat the process until we
	 * reach a palindrome that meets all of our criteria, then we exit.
	 * 
	 * This is significantly faster than attempting to multiply through until we find an answer, as it takes
	 * many many fewer computations which cuts our runtime down significantly. I understand this is
	 * a really, really odd way to do this so please email me at nysted@wisc.edu if you have any
	 * additional questions. 
	 * 
	 */
	public static boolean closeDivisors(long n)
	{
	    long a = Math.round(Math.sqrt(n));
	    while (n%a > 0 && a > 1000000)
	    {
	    	if(n/a > 9999999)
	    	{
	    		break;
	    	}
	    	a -= 1;
	    }
	    if(n%a == 0 && n/a < 9999999)
	    {
	    	return true;
	    }
	    return false;
	    
	}
	public static long nextLargestPalin(long palin)
	{
		long nextPalin = 0;
		String pal = Long.toString(palin);
		int palLength = pal.length()%2;
		if (palLength == 0)
		{
			pal = pal.substring(0, pal.length()/2);
		}
		else
		{
			pal = pal.substring(0,(pal.length()/2)+1);
		}
		int intPal = Integer.parseInt(pal);
		intPal-=1;
		StringBuilder sb = new StringBuilder(Integer.toString(intPal));
		nextPalin = Long.parseLong(sb.toString()+sb.reverse());
		return nextPalin;
	}
	public static long genPalin()
	{
		long longestPalindrome = 0;
		long startingPalin = 99999899899999L;
		longestPalindrome = startingPalin;
		while(!closeDivisors(longestPalindrome) && longestPalindrome > 0)
			{
				longestPalindrome = nextLargestPalin(longestPalindrome);
			}
		return longestPalindrome;
	}
	public static void main(String[] args)
	{
		long start = System.nanoTime();
		long palin = genPalin();
		long end = System.nanoTime();
		float time = (end-start);
		time = time/((float)1000000000);
		System.out.println(palin);
		System.out.println(time+"s");
	}
}
