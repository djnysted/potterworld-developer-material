from heapq import *
def manhattan(state):
   '''Calculates Manhattan distance for the tile's current position vs the proper
    position it should be in.'''
   d = 0
   for i in state: #iterates over full state.
       if i == 0:
           continue
       index = state.index(i)
       x1 = index//3 
       y1 = (index)%3
       x2 = (i-1)//3
       y2 = (i-1)%3
       d += abs(y2-y1)+abs(x2-x1) #Calculate manhattan from floor of x's and ys mod3
   return d   

def gen_succ(state):
    '''Identical to print state, minus a print function.'''
    z_index = state.index(0)
    state_list = []
    if (z_index - 1) >= 0 and z_index//3 == (z_index-1)//3:
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index-1] = state_copy[z_index-1], state_copy[z_index]
        state_list.append(state_copy)
    if (z_index + 1) < len(state) and z_index//3 == (z_index+1)//3:
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index+1] = state_copy[z_index+1], state_copy[z_index]
        state_list.append(state_copy)
    if (z_index + 3) < len(state):
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index+3] = state_copy[z_index+3], state_copy[z_index]
        state_list.append(state_copy)
    if (z_index - 3) >= 0:
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index-3] = state_copy[z_index-3], state_copy[z_index]
        state_list.append(state_copy)
        
    return sorted(state_list)
    
def print_succ(state):
    '''Generates all successors of a provided state then prints all of them out.'''
    z_index = state.index(0)
    state_list = []
    #Board can be thought of as 3 rows of 3, indexed 0-2 both directions. This code
    #ensures that none of the edge properties are violated when moving pieces.
    if (z_index - 1) >= 0 and z_index//3 == (z_index-1)//3:
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index-1] = state_copy[z_index-1], state_copy[z_index]
        state_list.append(state_copy)
    if (z_index + 1) < len(state) and z_index//3 == (z_index+1)//3:
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index+1] = state_copy[z_index+1], state_copy[z_index]
        state_list.append(state_copy)
    if (z_index + 3) < len(state):
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index+3] = state_copy[z_index+3], state_copy[z_index]
        state_list.append(state_copy)
    if (z_index - 3) >= 0:
        state_copy = state[:]
        state_copy[z_index], state_copy[z_index-3] = state_copy[z_index-3], state_copy[z_index]
        state_list.append(state_copy)
    
    state_list = sorted(state_list) #Sorts the states in the order specified
    for new_state in state_list:
        print(str(new_state) + " h=" + str(manhattan(new_state)))
    

def solve(state):
    '''Uses A* to find the shortest completion path for state.'''
    GOAL_STATE = [1,2,3,4,5,6,7,8,0]
    open_pq = []
    closed = []
    seen = {str(state)} 
    closed_index = 0
    h = manhattan(state)
    g = 0
    parent = -1
    heappush(open_pq, (g+h, state, (g,h,parent))) #Inserts the initial state as a node.
    max_len = 0
    while open_pq: #Runs while there are still nodes. If it's empty, we have failed.
        node = heappop(open_pq)
        state = node[1]
        seen.add(str(state))
        closed.append((node[2][2], state))
        closed_index += 1;
        if node[1] == GOAL_STATE: #If our state is the goal, we exit and print our path.
            break;
        succs = gen_succ(state) #Generate all successors. 
        for i in succs: #Checks if we've expanded each node, and adds it to the pq.
            if (not str(i) in seen):
                parent = closed_index-1
                g1 = node[2][0]+1
                h1 = manhattan(i)
                heappush(open_pq, (g1+h1, i, (g1,h1,parent)))
                
        if len(open_pq) > max_len: #Updates max_len if the length is greater than one seen before.
            max_len = len(open_pq)
    solution_list = []
    closed_index-=1
    while not closed_index == -1: #While we still have a parent that isn't the original node
        print_order = closed[closed_index]
        closed_index = print_order[0]
        solution_list.insert(0, print_order[1])
    
    moves = 0
    for i in solution_list: #Prints out our goal path in proper order, with h and moves.
        print(str(i) + " h=" + str(manhattan(i)) + " moves: " + str(moves))
        moves+=1
    print("Max queue length: " + str(max_len))
    return; 

