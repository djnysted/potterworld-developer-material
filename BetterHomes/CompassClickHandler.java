package leahnto.betterhomes;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;


public class CompassClickHandler implements Listener {
    BetterHomes plugin = BetterHomes.getInstance();
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        CompassGUI gui = plugin.getGui();
        ItemStack mainItem = e.getPlayer().getInventory().getItemInMainHand();
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR)|| e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (mainItem.getType() == Material.COMPASS) {
                if (mainItem.getItemMeta().hasDisplayName()) {
                    if (mainItem.getItemMeta().getDisplayName().equals(ChatColor.AQUA + "Wayfinder" + ChatColor.WHITE)) {
                        gui.openInventory(e.getPlayer());
                    }
                }
            }
        }
    }
}
