package leahnto.betterhomes;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GiveWayfinderCommandExecutor implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            ItemStack wayfinderItem = new ItemStack(Material.COMPASS);
            ItemMeta wayfinderInfo = wayfinderItem.getItemMeta();
            wayfinderInfo.setDisplayName(ChatColor.AQUA
                    + "Wayfinder" + ChatColor.WHITE);
            wayfinderItem.setItemMeta(wayfinderInfo);
            Player p = (Player) sender;
            p.getInventory().addItem(wayfinderItem);
            p.sendMessage("Giving Wayfinder...");
            return true;
        } else {
            System.out.println("You must run this command from in game, as a player!");
            return false;
        }
    }
}
