package leahnto.betterhomes;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class DelHomeCommandExecutor implements CommandExecutor {
    BetterHomes homePlugin = BetterHomes.getInstance();


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String homeName;
        if (args.length >= 1) {
            homeName = args[0];
        } else {
            homeName = "home";
        }
        homePlugin.reloadHomes();
        FileConfiguration config;
        config = homePlugin.getHomesConfig();
        Player p = (Player) sender;

        try {
            config.set(p.getName() + "." + homeName, null);
            p.sendMessage(ChatColor.YELLOW + "Home successfully deleted.");
        } catch (NullPointerException e) {
            e.printStackTrace();
            p.sendMessage("Error deleting home " + homeName);
            return false;
        }
        homePlugin.saveCustomConfig();
        return true;
    }
}
