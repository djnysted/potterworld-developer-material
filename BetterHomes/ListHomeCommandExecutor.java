package leahnto.betterhomes;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ListHomeCommandExecutor implements CommandExecutor {
    BetterHomes homePlugin = BetterHomes.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        FileConfiguration config;
        Player p = (Player) sender;
        homePlugin.reloadHomes();
        config = homePlugin.getHomesConfig();
        ConfigurationSection configSection = config.getConfigurationSection(p.getName());
        p.sendMessage(ChatColor.YELLOW+ p.getName() + " has homes:");
        for(String s : configSection.getKeys(false)) {
            Map<String, Object> subKeys = configSection.getConfigurationSection(s).getValues(false);
            List<Object> homeDistance = calcDistance((int)subKeys.get("x"), (int)subKeys.get("z"),
                    p.getLocation().getBlockX(), p.getLocation().getBlockZ());
            TextComponent message = new TextComponent(s + ChatColor.GRAY + " in world: "
                    + ChatColor.WHITE + subKeys.get("world")+ ChatColor.GRAY + "; "
                    + ChatColor.WHITE + homeDistance.get(0) + "m " + homeDistance.get(1));
            message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new Text(subKeys.get("description").toString())));
            p.spigot().sendMessage(message);
        }
        return true;
    }

    private List<Object> calcDistance(int x1, int z1, int x2, int z2) {
        List<Object> returnList = new ArrayList<Object>();
        int diffX = x1 - x2;
        int diffZ = z1 - z2;
        double distance = Math.sqrt((diffX * diffX) + (diffZ * diffZ));
        distance = Math.round(distance * 1000)/1000;
        String direction = "";
        if (diffZ < 0) direction += "North";
        else if (diffZ > 0) direction += "South";
        if (diffX < 0) {
            String toAppend = "West";
            if (!direction.equals("")) {
                toAppend = toAppend.toLowerCase();
            }
            direction += toAppend;
        }

        if (diffX > 0) {
            String toAppend = "East";
            if (!direction.equals("")) {
                toAppend = toAppend.toLowerCase();
            }
            direction += toAppend;
        }

        if (direction.equals("")) direction = "North";
        returnList.add(distance);
        returnList.add(direction);
        return returnList;
    }
}
