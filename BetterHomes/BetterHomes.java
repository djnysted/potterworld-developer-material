package leahnto.betterhomes;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public final class BetterHomes extends JavaPlugin {

    private FileConfiguration homesConfig = null;
    private File homesConfigFile = null;
    private static BetterHomes instance;
    private static CompassGUI gui;

    @Override
    public void onEnable() {
        reloadHomes();
        instance = this;
        gui = new CompassGUI();
        getServer().getPluginManager().registerEvents(new CompassClickHandler(), this);
        getServer().getPluginManager().registerEvents(gui, this);
        getServer().getPluginCommand("sethome").setExecutor(new SetHomeCommandExecutor());
        getServer().getPluginCommand("home").setExecutor(new HomeCommandExecutor());
        getServer().getPluginCommand("delhome").setExecutor(new DelHomeCommandExecutor());
        getServer().getPluginCommand("listhomes").setExecutor(new ListHomeCommandExecutor());
        getServer().getPluginCommand("compass").setExecutor(new GiveWayfinderCommandExecutor());
        System.out.println("BHLoading Complete");
    }
    public CompassGUI getGui() {
        return this.gui;
    }

    public void reloadHomes() {
        if (homesConfigFile == null) {
            homesConfigFile = new File(getDataFolder(), "homes.yml");
        }
        homesConfig = YamlConfiguration.loadConfiguration(homesConfigFile);
    }

    public FileConfiguration getHomesConfig() {
        if (homesConfig == null) {
            reloadHomes();
        }
        return homesConfig;
    }

    public void saveCustomConfig() {
        if (homesConfig == null || homesConfigFile == null) {
            return;
        }
        try {
            getHomesConfig().save(homesConfigFile);
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "Could not save config to " + homesConfigFile, ex);
        }
    }

    public static BetterHomes getInstance() {
        return instance;
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
