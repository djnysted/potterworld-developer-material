package leahnto.betterhomes;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;


public class SetHomeCommandExecutor implements CommandExecutor {

    BetterHomes homePlugin = BetterHomes.getInstance();
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        homePlugin.reloadHomes();
        String homeName;
        String description = "";
        FileConfiguration config;
        config = homePlugin.getHomesConfig();
        if (args.length >= 1) {
            homeName = args[0];
            for(int i = 1; i < args.length; i++) {
                description += args[i] + " ";
            }
            description = description.trim();
        } else {
            homeName = "home";
            description = "My Home";
        }
        if (description.equals("")) description = "My Home";

        Player p = (Player) sender;
        config.set(p.getName() + "." + homeName +  ".x", p.getLocation().getBlockX());
        config.set(p.getName() + "." + homeName + ".y", p.getLocation().getBlockY());
        config.set(p.getName() + "." + homeName + ".z", p.getLocation().getBlockZ());
        config.set(p.getName() + "." + homeName + ".world", p.getLocation().getWorld().getName());
        config.set(p.getName() + "." + homeName + ".description", description);
        homePlugin.saveCustomConfig();


        p.sendMessage("Home " + homeName + " added.");
        return true;
    }
}
