package leahnto.betterhomes;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class HomeCommandExecutor implements CommandExecutor {
    BetterHomes homePlugin = BetterHomes.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String homeName;
        if (args.length >= 1) {
            homeName = args[0];
        } else {
            homeName = "home";
        }
        homePlugin.reloadHomes();
        FileConfiguration config;
        config = homePlugin.getHomesConfig();
        Player p = (Player) sender;
        int x;
        int y;
        int z;
        try {
            x = (int) config.get(p.getName() + "." + homeName + ".x");
            y = (int) config.get(p.getName() + "." + homeName + ".y");
            z = (int) config.get(p.getName() + "." + homeName + ".z");
        } catch (NullPointerException e) {
            p.sendMessage("Error teleporting to home " + homeName + ", it may not exist.");
            return false;
        }
        p.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW + "Teleporting...");
        World world = Bukkit.getServer().getWorld((String) config.get(p.getName() + "." + homeName + ".world"));
        if (world == null) {
            return false;
        }
        p.teleport(new Location(world,x,y,z));
        p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 100, 0);

        return true;
    }
}
