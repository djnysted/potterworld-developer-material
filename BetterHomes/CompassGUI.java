package leahnto.betterhomes;

import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CompassGUI implements Listener {
    private final HashMap<String, Inventory> invPairing;
    BetterHomes homePlugin = BetterHomes.getInstance();

    public CompassGUI() {
        // Create a new inventory, with no owner (as this isn't a real inventory), a size of nine, called example
        invPairing = new HashMap<String, Inventory>();
    }

    protected ItemStack createGuiItem(final Material material, final String name, final String... lore) {
        final ItemStack item = new ItemStack(material, 1);
        final ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(name);
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }

    private int getInventorySize(int maxNodes) {
        if (maxNodes <= 0) return 9;
        int quotient = (int)Math.ceil(maxNodes / 9.0);
        return quotient > 5 ? 54: quotient * 9;
    }

    private void loadPlayerData(HumanEntity ent) {
        String playerName = ent.getName();
        FileConfiguration config = homePlugin.getHomesConfig();
        ConfigurationSection configSection = config.getConfigurationSection(playerName);
        if (configSection == null) {
            return;
        }
        int numNodes = configSection.getKeys(false).size();
        int invSize = getInventorySize(numNodes);
        Inventory inv = Bukkit.createInventory(null, invSize, "Homes");
        String playerWorld = ent.getWorld().getName();
        for(String s : configSection.getKeys(false)) {
            Map<String, Object> subKeys = configSection.getConfigurationSection(s).getValues(false);
            String x,y,z, world;
            x = subKeys.get("x").toString();
            y = subKeys.get("y").toString();
            z = subKeys.get("z").toString();
            world = subKeys.get("world").toString();
            Material mat;
            String trackMsg = "Click here to track to this location.";
            if (! playerWorld.equals(world)) {
                mat  = Material.RED_STAINED_GLASS_PANE;
                trackMsg = "Unable to track location: Wrong Dimension!";
            } else {
                mat = Material.GREEN_STAINED_GLASS_PANE;
            }
            inv.addItem(createGuiItem(mat, ChatColor.DARK_AQUA + s,
                    "§a" + x + ", " + y + ", " + z + ", " + world,
                    "§b" + trackMsg));
        }
        invPairing.put(playerName, inv);

    }

    // You can open the inventory with this
    public void openInventory(final HumanEntity ent) {
        String playerName = ent.getName();
        loadPlayerData(ent);
        ent.openInventory(invPairing.get(playerName));
    }

    // Check for clicks on items
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        if (!invPairing.containsValue(e.getInventory())) return;
        final Player p = (Player) e.getWhoClicked();
        String playerName = p.getName();
        e.setCancelled(true);

        final ItemStack clickedItem = e.getCurrentItem();

        // verify current item is not null
        if (clickedItem == null || clickedItem.getType().isAir()) return;

        if(!clickedItem.getItemMeta().hasDisplayName()) {
            return;
        }
        FileConfiguration config = homePlugin.getHomesConfig();
        ConfigurationSection configSection = config.getConfigurationSection(playerName);
        if (configSection == null) {
            return;
        }
        String itemName = clickedItem.getItemMeta().getDisplayName();
        itemName = ChatColor.stripColor(itemName);
        ConfigurationSection home = configSection.getConfigurationSection(itemName);
        Map<String, Object> subKeys = home.getValues(false);
        double x,y,z;
        x = (int) subKeys.get("x");
        y = (int) subKeys.get("y");
        z = (int) subKeys.get("z");
        String worldName = subKeys.get("world").toString();
        if (clickedItem.getItemMeta().getLore().get(1).contains("Wrong Dimension!")) {
            return;
        }
        // Using slots click is a best option for your inventory click's
        World world = Bukkit.getServer().getWorld(worldName);
        p.setCompassTarget(new Location(world, x,y,z));
        p.sendMessage("Now tracking!");
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (invPairing.containsValue(e.getInventory())) {
            e.setCancelled(true);
        }
    }
}
