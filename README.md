# Potterworld Developer Material



## BetterHomes
(Circa Sept 2021)
Plugin suggested by a friend in order to improve my spigot skills. Recreates essentials /home functionality but also adds additional features.
For instance, all homes are shown in distance from player in the listhomes menu, and a special compass is implemented to be able to track to player homes. This is an alpha version of the plugin but all major functionality is in place.

## PalindromesFast
(Circa Feb 2021)
A solution to a Project Euler question- the problem was to find the largest two factors of a 7 digit palindrome. This is a very fast and efficient solution which essentially turns the problem on it's head by checking palindromes for factors rather than the other way around. More information in the file. 

## ninePuzzleSolver
(Circa October 2020)
A puzzle which uses A* search in order to find the optimal solution to a 9 tile sliding puzzle.

## genTPbom
(Ongoing, Circa July 2021)
I'm unable to share source code for this as it's an ongoing proprietary work project, but it's something I can talk about; this program essentially serves as a parser for an ODT file and constructs a bill of materials product. Skills learned: Created a custom XML parser, did lots of work with filepaths, lots of server queries. 



